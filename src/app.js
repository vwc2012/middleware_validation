require('./config/dotenv')();
require('./config/sequelize');
const express = require('express');
const routes = require("./routes/routes");
const app = express();
const port = process.env.PORT;
const passport = require('passport');
require('./strategies/jwtStrategy')(passport);

app.get('/', (req, res) => {
  res.send('Hello World!')
});


app.use(passport.initialize());
app.use(passport.session());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(routes);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
    