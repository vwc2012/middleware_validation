const Phone = require('../models/Phone');
const {validationResult} = require('express-validator');
const {Op} = require('sequelize');

const index = async(req, res) =>{
    try{
        const phones = await Phone.findAll();
        return res.status(200).json({phones});
    } catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const phone = await Phone.findByPk(id);
        return res.status(200).json({phone});
    }catch(err){
        return res.status(500).json({err});
    }
};


const search = async(req, res) =>{
    const {model_name, chips_number} = req.body;
    try{
        const result = await Phone.findAll({
            where:{
                [Op.and]:{
                    model_name: {
                        [Op.like]: '%'+model_name+'%'
                    },
                    chips_number: {
                        [Op.like]: chips_number
                    }
                }
            }
        });
        return res.status(200).json({result});
    }catch{
        res.status(500).json({error: err});
    }
}

const create = async(req,res) => {
    try{
          validationResult(req).throw(); //validação
          const phone = await Phone.create(req.body);
          console.log(req.body);
          return res.status(201).json({message: "Telefone cadastrado com sucesso!", phone: phone});
      }catch(err){
          res.status(500).json({error: err});
      }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Phone.update(req.body, {where: {id: id}});
        if(updated) {
            const phone = await Phone.findByPk(id);
            return res.status(200).send(phone);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Telefone não encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Phone.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Usuario deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Usuario não encontrado.");
    }
};

module.exports = {
    update,
    destroy,
    create,
    index,
    show,
    search
}
