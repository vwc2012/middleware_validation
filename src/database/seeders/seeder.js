require('../../config/dotenv')();
require('../../config/sequelize');

const seedPhone = require('./PhoneSeeder');
const seedUser = require('./UserSeeder');

(async () => {
  try {
    await seedUser();
    await seedPhone();

  } catch(err) { console.log(err) }
})();
