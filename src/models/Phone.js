const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Phone = sequelize.define('Phone', {
    model_name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    manufacturer: {
        type: DataTypes.STRING,
        allowNull: false
    },

    date_of_acquire: {
        type: DataTypes.DATEONLY,
        allowNull: true
    },

    has_nfc: {
        type: DataTypes.BOOLEAN,
        allowNull: true

    },

    chips_number: {
        type: DataTypes.INTEGER,
        allowNull: true
    }


});

Phone.associate = function(models){
   Phone.belongsTo(models.User); 
}

module.exports= Phone;