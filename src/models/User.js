const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    date_of_birth: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },

    phone_number: {
        type: DataTypes.STRING
    },

    gender: {
        type: DataTypes.STRING
    },

    type_user: {
        type: DataTypes.BOOLEAN
    },
    
    salt: {
        type: DataTypes.STRING
    },

    hash: {
        type: DataTypes.STRING
    }


});

User.associate = function(models){
    User.hasMany(models.Phone);
}

module.exports= User;