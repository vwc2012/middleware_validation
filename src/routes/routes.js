const router = require("express").Router();
const validator = require("../config/validator");
const PhoneController = require("../controllers/PhoneController");
const UserController = require("../controllers/UserController");
const adminMiddleware = require("../middlewares/admin");
const AuthController = require('../controllers/AuthController');
const setAuthorizationHeader = require('../middlewares/token');
const bodyParser = require('body-parser');
const urlEncodeParser = bodyParser.urlencoded({extended: false});
const passport = require('passport');

router.post('/phone', validator.validationPhone('create'), PhoneController.create);
router.get('/phone/:id', adminMiddleware, PhoneController.search);
router.post('/register', urlEncodeParser , AuthController.register);
router.post('/login', AuthController.login);
router.post('/user', validator.validationUser('create'), UserController.create);

module.exports = router;